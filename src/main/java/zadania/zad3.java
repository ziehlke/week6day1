package zadania;
//    Stwórz oddzielnego maina, a w nim kolejną listę String'ów. Wykonaj zadania:
//            - dodaj do listy elementy 10030, 3004, 4000, 12355, 12222, 67888, 111200, 225355, 2222, 1111, 3546, 138751, 235912 (jako stringi) (dodaj je posługując się metodą Arrays.asList())

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class zad3 {

    public static void main(String[] args) {

        List<String> list = new ArrayList<>(Arrays.asList("10030", "3004", "4000", "12355", "12222", "67888", "111200", "225355", "2222", "1111", "3546", "138751", "235912"));
//        - określ indeks elementu 138751 posługując się metodą indexOf
        System.out.println(list.indexOf("138751"));


//        - sprawdź czy istnieje na liście obiekt 67888 metodą contains (wynik wypisz na ekran)
        System.out.println(list.contains("67888"));


//        - sprawdź czy istnieje na liście obiekkt 67889 metodą contains (wynik wypisz na ekran)
        System.out.println(list.contains("67889"));


//        - usuń z listy obiekt 67888 oraz 67889 (metoda remove)
        list.remove("67888");
        list.remove("67889");

//        - wykonaj ponownie powyższe sprawdzenia.
        System.out.println(list.contains("67888"));
        System.out.println(list.contains("67889"));


//                - iteruj całą listę, wypisz elementy na ekran (a. w jednej linii, b. każdy element w oddzielnej linii).
//                Sprawdź działanie aplikacji.

        String strA = "";
        for (int i = 0; i < list.size(); i++) {

            strA += list.get(i);
            System.out.println(list.get(i));

        }
        System.out.println(strA);


    }

}
