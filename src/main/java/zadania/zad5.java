package zadania;

//5. Stwórz klasę Student która posiada pola:
//        nr indeksu
//        imie
//        nazwisko
//        płeć (wartość enum)
//
//


import java.util.ArrayList;
import java.util.List;

public class zad5 {


    public static void main(String[] args) {

//        Stwórz instancję kolekcji typu ArrayList, która zawiera obiekty klasy Student
//        w mainie dodaj do kolekcji kilku studentów (dodaj je ręcznie - wpisz cokolwiek).
        List<Student> studentList = new ArrayList<>();

        Student student1 = new Student();
        student1.index = 0;
        student1.imie = "imie1";
        student1.nazwisko = "nazwisko1";
        student1.plec = Plec.KOBIETA;

        Student student2 = new Student();
        student2.index = ++student1.index;
        student2.imie = "imie2";
        student2.nazwisko = "nazwisko2";
        student2.plec = Plec.MEZCZYZNA;

        Student student3 = new Student();
        student3.index = ++student2.index;
        student3.imie = "imie3";
        student3.nazwisko = "nazwisko3";
        student3.plec = Plec.INNY;

        Student student4 = new Student();
        student4.index = ++student3.index;
        student4.imie = "imie4";
        student4.nazwisko = "nazwisko4";
        student4.plec = Plec.MEZCZYZNA;

        studentList.add(student1);
        studentList.add(student2);
        studentList.add(student3);
        studentList.add(student4);


//        a. Spróbuj wypisać elementy listy stosując zwykłego "sout'a"


        for (int i = 0; i < studentList.size(); i++) {
            System.out.println(studentList.get(i));
        }

//        b. Spróbuj wypisać elementy stosując pętlę foreach

        for (Student s: studentList
        ) {
            System.out.println(s);
        }


//        c. Wypisz tylko kobiety

        for (Student s: studentList
        ) {
            if (s.plec == Plec.KOBIETA) System.out.println(s);
        }

//        d. Wypisz tylko mężczyzn

        for (Student s: studentList
        ) {
            if (s.plec == Plec.MEZCZYZNA) System.out.println(s);
        }
//        e. Wypisz tylko indeksy osób

        for (Student s: studentList
        ) {
            System.out.println(s.index);
        }

    }
}
