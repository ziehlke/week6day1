package zadania.streams.lambdas;

import zadania.zad6.Student;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {

//    PREDICATE
//1. Napisz funkcję, która przyjmuje liczbę i sprawdza, czy liczba ta jest parzysta.


    public static void main(String[] args) {
        Predicate<Integer> predicatePar = n -> {
            return n % 2 == 0;
        };
        System.out.println(predicatePar.test(1));
        System.out.println(predicatePar.test(2));


//            2. Napisz funkcję, która przyjmuje listę Stringów i sprawdza, czy wśród nich jest
//              String zawierający słowo "gotchya".

        Predicate<List<String>> hasGotchya = list -> {
            return list.contains("gotchya");

        };

//        System.out.println(hasGotchya.test(Arrays.asList("adfagotchyadsgsg","adfadsfdsf","gotchya")););


//            3. Napisz klasę person, która ma pola: firstName, lastName, age oraz isMale. Napisz:
//    a) funkcję, która zwraca true, jeśli Person jest mężczyzną

        Predicate<Person> jestMezczyzna = s -> {
            return s.isMale();
        };
        System.out.println();


//    b) funkcję, która zwraca true, jeśli Person ma na imię "Jacek"

        Predicate<Person> imieJacek = p -> p.getFirstName().equals("Jacek");

//    c) funkcję, która zwraca true, jeśli jest osoba jest pełnoletnia.

        Predicate<Person> jestPelnoletnia = p -> p.getAge() >= 18;


//4.  Napisz funkcję, która zwraca true, jeśli w kolekcji <Person> znajduje się dorosły mężczyzna,
//     który ma na imię jacek.

        Predicate<Collection<Person>> collectionPredicate = cp -> {
            for (Person p : cp
            ) {
                if (imieJacek.test(p) && jestPelnoletnia.test(p) && jestMezczyzna.test(p)) return true;
            }
            return false;
        };


        Person person1 = new Person("Jacek", "Kowalski", 18, true);
        Person person2 = new Person("Jacek", "Górski", 15, true);
        Person person3 = new Person("Andżelika", "Dżoli", 25, false);
        Person person4 = new Person("Wanda", "Ibanda", 12, false);
        Person person5 = new Person("Marek", "Marecki", 17, true);
        Person person6 = new Person("Johny", "Brawo", 25, true);
        Person person7 = new Person("Stary", "Pan", 80, true);
        Person person8 = new Person("Newbie", "Noob", 12, true);
        Person person9 = new Person("Newbies", "Sister", 19, false);


        List<Person> personList = new ArrayList<>(Arrays.asList(
                person1,
                person2,
                person3,
                person4,
                person5,
                person6,
                person7,
                person8,
                person9
        ));


//        1.Napisz klasę person, która ma pola: firstName, lastName, age oraz isMale.
//        Mając listę osób i korzystając ze streamów:
//        a) uzyskaj listę mężczyzn


        List<Person> mezczyzni = personList.stream()
                .filter(p -> p.isMale())
                .collect(Collectors.toList());


//        b) uzyskaj listę dorosłych kobiet

        List<Person> dorosleKobiety = personList.stream()
                .filter(p -> !p.isMale())
                .filter(p -> p.getAge() >= 18)
                .collect(Collectors.toList());

//        c) uzyskaj Optional<Person> z dorosłym Jackiem

        Optional<Person> doroslyJacek = personList.stream()
                .filter(p -> p.getFirstName().equals("Jacek"))
                .filter(Person::isMale).findFirst();


//        d) uzyskaj listę wszystkich nazwisk osób, które są w przedziale wiekowym: 15-19

        List<String> nazwiskaPietnascieDoDziewietnascie = personList.stream()
                .filter(person -> person.getAge() >= 15)
                .filter(person -> person.getAge() <= 19)
                .map(Person::getLastName)
                .collect(Collectors.toList());

//        e)* uzyskaj sumę wieku wszystkich osób

        int sumaWiekuWszystkich = personList.stream()
                .mapToInt(p -> p.getAge())
                .sum();


//        f)* uzyskaj średnią wieku wszystkich mężczyzn

        OptionalDouble sredniaWszystkichMezczyzn = personList.stream()
                .filter(p -> p.isMale())
                .mapToInt(Person::getAge)
                .average();

        if (sredniaWszystkichMezczyzn.isPresent())
            System.out.println("Srednia: " + sredniaWszystkichMezczyzn.getAsDouble());

//        g)** znajdź nastarszą osobę w liście


        Optional<Person> najstarszaOsoba = personList.stream()
                .sorted((o1, o2) -> {
                    return o1.getAge() > o2.getAge() ? 1 : (o1.getAge() < o2.getAge()) ? -1 : 0;
                })
                .findFirst();

        System.out.println(najstarszaOsoba.get());

        System.out.println("----------------------------------------------------------------");


//        2. Stwórz klasę Programmer, która ma klasę Person jako pole.
//          Poza tym, posiada listę języków, którymi się posługuje.
//          Mogą być one reprezentowane przez klasę String.

        List<String> languages1 = Arrays.asList("Java;Cobol;Cpp;Lisp".split(";"));
        List<String> languages2 = Arrays.asList("Java;Lisp".split(";"));
        List<String> languages3 = Arrays.asList("Java;Cobol;Cpp;Lisp;C#".split(";"));
        List<String> languages4 = Arrays.asList("C#;C;Cpp".split(";"));
        List<String> languages5 = Arrays.asList("Java;Assembler;Scala;Cobol".split(";"));
        List<String> languages6 = Arrays.asList("Java;Scala".split(";"));
        List<String> languages7 = Arrays.asList("C#;C".split(";"));
        List<String> languages8 = Collections.emptyList();
        List<String> languages9 = Arrays.asList("Java");
        Programmer programmer1 = new Programmer(person1, languages1);
        Programmer programmer2 = new Programmer(person2, languages2);
        Programmer programmer3 = new Programmer(person3, languages3);
        Programmer programmer4 = new Programmer(person4, languages4);
        Programmer programmer5 = new Programmer(person5, languages5);
        Programmer programmer6 = new Programmer(person6, languages6);
        Programmer programmer7 = new Programmer(person7, languages7);
        Programmer programmer8 = new Programmer(person8, languages8);
        Programmer programmer9 = new Programmer(person9, languages9);
        List<Programmer> programmers = Arrays.asList(programmer1, programmer2, programmer3, programmer4,
                programmer5, programmer6, programmer7, programmer8, programmer9);


//          Mając listę programistów i korzystając ze streamów:
//        a) uzyskaj listę programistów, którzy są mężczyznami

        List<Programmer> programisciMezczyzni = programmers.stream()
                .filter(p -> p.getPerson().isMale())
                .collect(Collectors.toList());


//        b) uzyskaj listę niepełnoletnich programistów (obydwóch płci), którzy piszą w Cobolu

        List<Programmer> programisciCobol = programmers.stream()
                .filter(p -> p.getLanguages().contains("Cobol"))
                .collect(Collectors.toList());

//        c) uzyskaj listę programistów, którzy znają więcej, niż jeden język programowania

        List<Programmer> programisciWiecejNizJede = programmers.stream()
                .filter(p -> p.getLanguages().size() > 1)
                .collect(Collectors.toList());

//        d) uzyskaj listę programistek, które piszą w Javie i Cpp

        List<Programmer> programistkiJavaICpp = programmers.stream()
                .filter(p -> !p.getPerson().isMale())
                .filter(p -> p.getLanguages().contains("Java"))
                .filter(p -> p.getLanguages().contains("Cpp"))
                .collect(Collectors.toList());


//        e) uzyskaj listę męskich imion

        List<String> imnionaMeskie = programmers.stream()
                .filter(p -> p.getPerson().isMale())
                .map(p -> p.getPerson().getFirstName())
                .collect(Collectors.toList());

//        f) uzyskaj set wszystkich języków opanowanych przez programistów

        Set<String> wszystkieJezyki = programmers.stream()
                .map(p -> p.getLanguages())
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

//        g) uzyskaj listę nazwisk programistów, którzy znają więcej, niż dwa języki

        List<String> programisciZnajdacyWiecejNizDwa = programmers.stream()
                .filter(p -> p.getLanguages().size() > 2)
                .map(p -> p.getPerson().getLastName())
                .collect(Collectors.toList());


//        h) sprawdź, czy istnieje chociaż jedna osoba, która nie zna żadnego języka

        boolean czyJestOsobaCoNieZna = programmers.stream()
                .filter(p -> p.getLanguages().size() < 1)
                .count() == 0;


//        i)* uzyskaj ilość wszystkich języków opanowanych przez programistki


        int iloscJezykowProgramistek = programmers.stream()
                .filter(p -> !p.getPerson().isMale())
                .mapToInt(p -> p.getLanguages().size())
                .sum();


//        j) uzyskaj średnią ilość opanowanych języków przez programistki

        OptionalDouble sredniaIloscJezykowProgramistek = programmers.stream()
                .filter(p -> !p.getPerson().isMale())
                .mapToDouble(p -> p.getLanguages().size())
                .average();

        if (sredniaIloscJezykowProgramistek.isPresent()) System.out.println(sredniaIloscJezykowProgramistek);


//        k) uzyskaj płeć programisty, który zna największą ilość języków

        Optional<Programmer> wymiatacz = programmers.stream()
                .sorted((o1, o2) -> {
                    return o1.getLanguages().size() > o2.getLanguages().size() ?
                            1 : (o1.getLanguages().size() < o2.getLanguages().size()) ? -1 : 0;
                })
                .findFirst();


        if (wymiatacz.isPresent()) {
            System.out.println(wymiatacz.map(p -> p.getPerson().isMale()));
        }


//        l) zwroc listę językow posortowanych wedlug powszechnosci
        // TODO: to jest skomplikowane

        Map<String, Long> popularneJezyki = programmers.stream()
                .map(p -> p.getLanguages())
                .flatMap(Collection::stream)
                .collect(Collectors.groupingBy(p -> p, LinkedHashMap::new, Collectors.counting())) //mapa jezyki / ilosc wystapien
                .entrySet().stream() // rozbicie na entry est
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())) // sortujemy po wartosciach
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldVal, newVal) -> oldVal, LinkedHashMap::new));

        // kluczem zostaje dalej klucz
        // wartosc dalej wartosc
        // mapping wartosc na wartosc
        // zebranie linekdHaskMap (aby zachowac kolejnosc)

        System.out.println(popularneJezyki);


//
//        3*.Używając streamów znajdź długość najdłuższej linii w wybranym przez ciebie pliku.
//        Zauważ, że klasa BufferedReader ma metodę stream().
//
//        4*. Zamien poniższy kod:
//
//        List<Album> favs = new ArrayList<>();
//        for (Album a : albums) {
//            boolean hasFavorite = false;
//            for (Track t : a.tracks) {
//                if (t.rating >= 4) {
//                    hasFavorite = true;
//                    break;
//                }
//            }
//            if (hasFavorite)
//                favs.add(a);
//        }
//        Collections.sort(favs, new Comparator<Album>() {
//            public int compare(Album a1, Album a2) {
//                return a1.name.compareTo(a2.name);
//            }});
//
//         ..na kod z wykorzystaniem streamów.


    }
}
