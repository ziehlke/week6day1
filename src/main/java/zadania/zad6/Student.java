package zadania.zad6;

//Klasa Student powinna:
//        - posiadać listę ocen studenta (List<Double>)
//        - posiadać (pole) numer indeksu studenta (String)
//        - posiadać (pole) imię studenta
//        - posiadać (pole) nazwisko studenta


import java.util.List;

public class Student {
    private List<Double> oceny;
    private String index, imie, nazwisko;

    public Student() {
    }

    public Student(List<Double> oceny, String index, String imie, String nazwisko) {
        this.oceny = oceny;
        this.index = index;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public Student(String index, String imie, String nazwisko) {
        this.index = index;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public List<Double> getOceny() {
        return oceny;
    }

    public void setOceny(List<Double> oceny) {
        this.oceny = oceny;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }


    public Double obliczSrednia(){
        double suma = 0.0;

        for (Double ocena: oceny){
            suma += ocena;
        }

        return suma/oceny.size();
    }




}
