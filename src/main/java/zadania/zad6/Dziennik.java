package zadania.zad6;
//        Dziennik powinna:


import java.util.*;

public class Dziennik {
    //        - posiadać (jako pole) listę Studentów.
    private List<Student> students;


    //        - posiadać metodę 'dodajStudenta(Student):void' do dodawania nowego studenta do dziennika
    public void dodajStudenta(Student student) {
        students.add(student);
    }

    //        - posiadać metodę 'usuńStudenta(Student):void' do usuwania studenta
    public void usunStudenta(Student student) {
        students.remove(student);
    }

    //        - posiadać metodę 'usuńStudenta(String):void' do usuwania studenta po jego numerze indexu
    public void usunStudenta(String index) {
        List<Student> kopia = new ArrayList<>(students); // robi kopie, bo przy foreachu nei mozna usuwac..

        for (Student s : kopia
        ) {
            if (s.getIndex().equals(index)) {
                students.remove(s);
            }
        }
    }

//        - posiadać metodę 'zwróćStudenta(String):Student' która jako parametr przyjmuje numer indexu studenta, a w wyniku zwraca konkretnego studenta.

    public Optional<Student> zwrocStudenta(String index) {
        for (Student s : students
        ) {
            if (s.getIndex().equals(index)) {
                return Optional.of(s);
            }
        }
        return Optional.empty();
    }


//        - posiadać metodę 'podajŚredniąStudenta(String):double' która przyjmuje indeks studenta i zwraca średnią ocen studenta.

    public OptionalDouble podajSredniaStudenta(String index) {
        Optional<Student> studentOptional = zwrocStudenta(index);
        if (studentOptional.isPresent()) {
            double srednia = studentOptional.get().obliczSrednia();
            return OptionalDouble.of(srednia);
        }
        return OptionalDouble.empty();
    }

    //        - posiadać metodę 'podajStudentówZagrożonych():List<Student>'
    public List<Student> podajZagrozonych() {
        List<Student> zagrozeni = new ArrayList<>();

        for (Student s : students
        ) {
            if (s.obliczSrednia() <= 2.0) {
                zagrozeni.add(s);
            }

        }
        return zagrozeni;

    }


//    //        - posiadać metodę 'posortujStudentówPoIndeksie():List<Student>' - która sortuje listę studentów po numerach indeksów, a następnie zwraca posortowaną listę.
//    public List<Student> posortujPoIndexie() {
//
//        List<Student> copy = new ArrayList<>(students);
//
//        copy.sort(new Comparator<Student>() {
//            @Override
//            public int compare(Student o1, Student o2) {
//                if (o1.getIndex().equals(o2.getIndex())) return 0;
//
//                //TODO: dokonczyc comparator
//            }
//        });
//
//        return null;
//    }


}
