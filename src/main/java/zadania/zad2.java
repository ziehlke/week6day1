package zadania;

//Stwórz oddzielnego maina, a w nim kolejną listę integerów. Wykonaj zadania:
//        - dodaj do listy 10 liczb losowych
//        - oblicz sumę elementów na liście (wypisz ją)
//        - oblicz średnią elementów na liście (wypisz ją)
//        - podaj medianę elementów na liście (wypisz ją) (Collections.sort( listaDoPosortowania) - sortuje listę)


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class zad2 {

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        Random random = new Random();

        int sum = 0;
        int number = 0;

        for (int i = 0; i < 10; i++) {
            number = random.nextInt(10);
            list.add(number);
            sum += number;
        }

        System.out.println("Lista: " + list);
        System.out.println("Sum: " + sum);
        System.out.println("Average: " + sum / list.size());
        System.out.println("Mediana: " + findMedian(list));


//        - znajdź największy oraz najmniejszy element na liście
//        (znajdź go posługując się pętlą for) - wypisz indeks elementu

        int min = list.get(0);
        int indexMin = 0;
        int max = list.get(0);
        int indexMax = 0;

        for (int i = 1; i < list.size(); i++) {
            if (list.get(i) < min) {
                min = list.get(i);
                indexMin = i;
            }
            if (list.get(i) > max) {
                max = list.get(i);
                indexMax = i;
            }

        }

        System.out.println("index Min / Max: " + indexMin + "/" + indexMax);


//        - znajdź największy oraz najmniejszy element na liście
//        (znajdź go pętlą for, a następnie określ index posługując się metodą indexOf)


        System.out.println("minIndex inaczej: " + list.indexOf(min));
        System.out.println("maxIndex inaczej: " + list.indexOf(max));


    }

    private static double findMedian(List<Integer> list) {

        List<Integer> kopia = new ArrayList<>(list);
        Collections.sort(kopia);
        System.out.println("Sorted list: " + kopia);

        if (kopia.size() % 2 == 1) {
            // nieparzysta ilosc elementow w liscie
            // np. 9 -> 0 1 2 3 - 4 - 5 6 7 8
            return kopia.get(kopia.size() / 2);
        } else {
            // nieparzysta ilosc elementow w liscie
            // np. 8 -> 0 1 2 - 3  4 - 5 6 7
            return (kopia.get(kopia.size() / 2) + kopia.get((kopia.size() / 2) - 1)) / 2.0;

        }
    }

}
